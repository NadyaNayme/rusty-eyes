# ABANDONED

This project has been abandoned. With help from an [Anon from 4chan's /g/](https://boards.4chan.org/g/thread/66040345#p66063072), I have discovered a fix to the Thumbnail Browse mode. Running the program with compatibility mode for Vista SP2 will prevent Impression:Eyes from crashing.

To hell with the Win32 API.

# Rusty:Eyes

Recreating Impression:Eyes in Rust. Impression:Eyes was a minimalistic image browser coded by Utilhaven. In early 2013, Utilhaven and his website disappeared. It is a fantastic image browser with only a few small flaws, one being that the Thumbnail Browse mode occasionally crashes the program.

A video displaying Impression:Eyes functionality can be [viewed here](https://pomf.pyonpyon.moe/txfklm.mp4).

## Installation

A precompiled executable binary for Rusty:Eyes can be [downloaded here](https://nadyanay.me/software/installs/rusty_eyes.exe). Even though it doesn't actually do anything. Honestly - it does nothing right now.

The file should have the following MD5 and SHA1 hashes, but since this process isn't automated yet it might not.

MD5: `f3590d58df06c226c95ec3668a01986f`
SHA1: `d6f3b935934575430ef676eef8dd6ce6cd0b6b37`

Alternatively, you can clone the repo and it should build from source just fine.

## ToDo List

* [x] Release Rusty:Eyes 0.1.0 even though it's completely useless
* [x] Open a Window (seriously, why was this so hard?)
* [ ] Find a graphics friend willing to donate a nice icon or make one myself
* [ ] Make Rusty:Eyes actually do something when you select a file
* [ ] Implement basic functionality like rotating the image or saving it as another file type
* [ ] Get rid of the chrome
* [ ] Implement Thumbnail Browsing mode which is why I'm even recoding Impression:Eyes in the first place.
