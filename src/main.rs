// #![windows_subsystem = "windows"]
#![allow(unused_variables)]

#[macro_use]
extern crate native_windows_gui as nwg;

use std::path::Path;

use nwg::{dispatch_events, fatal_message, Event, Ui};

#[derive(Debug, Clone, Hash)]
pub enum AppId {
    // Controls
    MainWindow,
    BrowseFile,
    FileDialog,

    // Events
    GetFile,

    // Resources
    MainFont,
    TextFont,
}

use AppId::*;

nwg_template!(
    head: setup_ui<AppId>,
    controls: [
        (MainWindow, nwg_window!( title="Rusty:Eyes"; size=(100, 30); position=(nwg::constants::CENTER_POSITION, nwg::constants::CENTER_POSITION); resizable=true )),
        (BrowseFile, nwg_button!(parent=MainWindow; text="Open Image")),
        (FileDialog, nwg_filedialog!(parent=Some(MainWindow); filters=Some("Test(*.jpg;*.jpeg;*.png;*.gif)|Any(*.*)")))
    ];
    events: [

        // When the "Open Image" button is clicked open up a File Dialog
        // to allow the user to select an image.
        (BrowseFile, GetFile, Event::Click, |ui,_,_,_| {
            let dialog = nwg_get_mut!(ui;(FileDialog, nwg::FileDialog));
            dialog.run();
            let user_input = dialog.get_selected_item();
            match user_input {
                Ok(val) => {
                    let absolute_path = Path::new(&val);

                    // There has to be a better way of doing this than unwrapping twice
                    let image_parent_directory = &absolute_path.parent().unwrap().to_str().unwrap();
                    let image_file_name = &absolute_path.file_stem().unwrap().to_str().unwrap();
                    let image_file_extension = &absolute_path.extension().unwrap().to_str().unwrap();
                    println!("User selected file: {}", &val);
                    println!("The current directory is: {:?}", image_parent_directory);
                    println!("The file name is: {:?}", image_file_name);
                    println!("The file extension type is: {:?}", image_file_extension);
                }
                Err(e) => println!("No file selected."),
            }

        })

        // TODO: Paint an opened image to the screen
        //       which is the point of this program.

        // TODO: Add the following functionality for keybinds

        // Bind ESC to exit the program
        // Bind space to go to Next Image
        // Bind Backspace and Mousewheel Up to go to Previous Image
        // Bind LEFT_ARROW and Mousewheel Down to rotate image -90 degrees
        // Bind RIGHT_ARROW to rotate image 90 degrees
        // Bind h to rotate image horizontally
        // Bind v to rotate image vertically
        // Bind O and Ctrl+O to open a file
        // Bind Enter to toggle between Original Size and Full Screen size
        // Bind Ctrl+Mousewheel Up to Zoom In
        // Bind Ctrl+Mousewheel Down to Zoom Out
        // Bind Ctrl+L to open File Directory in Explorer
        // Bind F7 to toggle Alpha Transparency Mode
        // Bind F8 to toggle Always on Top window
        // Bind Ctrl+N to open a new instance of Rusty:Eyes
    ];
    resources: [
        (MainFont, nwg_font!(family="Arial"; size=27)),
        (TextFont, nwg_font!(family="Arial"; size=17))
    ];
    values: []
);

fn main() {
    let app: Ui<AppId>;

    match Ui::new() {
        Ok(_app) => {
            app = _app;
        }
        Err(e) => {
            fatal_message("Fatal Error", &format!("{:?}", e));
        }
    }

    if let Err(e) = setup_ui(&app) {
        fatal_message("Fatal Error", &format!("{:?}", e));
    }

    dispatch_events();
}
